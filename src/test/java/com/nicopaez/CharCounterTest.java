package com.nicopaez;


import org.junit.Test;

import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;

public class CharCounterTest {

    @Test
    public void whenInputIsEmptyResultIsEmpty() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> result =  counter.countAll("");
        assert(result.isEmpty());
    }
}
